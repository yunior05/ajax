
function loadData() {

    var $body = $('body');
    var $wikiElem = $('#wikipedia-links');
    var $nytHeaderElem = $('#nytimes-header');
    var $nytElem = $('#nytimes-articles');
    var $greeting = $('#greeting');

    // clear out old data before new request
    $wikiElem.text("");
    $nytElem.text("");

    // load streetview


    // YOUR CODE GOES HERE!

    var streetStr = $("#street").val();
    var cityStr = $("#city").val();
    var adress = streetStr + ", " + cityStr;

   

    var url = "https://api.nytimes.com/svc/search/v2/articlesearch.json";
    url += '?' + $.param({
        'api-key': "df0b2318e8814d9f957dfaa3cb7c15fc",
        'q': adress
    });
    $.ajax({
        url: url,
        method: 'GET',
    }).done(function(result) {
        var articles = result.response.docs;
        for (i = 0; i < articles.length; i++) {
            var article = articles[i];
            $nytElem.append('<li class = "article"> '+'<a href= "'
            +article.web_url+'" > '+article.headline.main+'</a>'
            + '<p>'+article.snippet+'</p></li>');
            
        }

        $greeting.text("So, you want to live in " + adress + "?");
    }).fail(function(err) {
        $nytElem.text("New York Times Articles could not be loaded");
        $greeting.text("Location no found ");
    });
    return false;
};

$('#form-container').submit(loadData);
